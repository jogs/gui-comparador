export default class Converter {
  constructor (type = ',') {
    this.regexp = new RegExp((type === ' ') ? ' ' : `\\${type}`)
  }
  parseValue (value) {
    console.log(value)
    if (/\-?(?:0|[1-9][\d]*)(?:\.?[\d]+)?(?:[eE]?[\+\-]?[\d]*)?/.test(value)) {
      return Number(value)
    } else {
      switch (value) {
        case 'true':
          return true
        case 'false':
          return false
        case 'null':
          return null
        default:
        return value
      }
    }
  }
  parse (text) {
    let tmp = text.split(/\n/).map((element) => element.split(this.regexp))
    if (tmp.length < 2) return false
    else if (tmp.length === 2 || (tmp.length === 3 && tmp[2][0] === '')) {
      let json = {}
      tmp[0].map((key, index) => {
        json[key] = this.parseValue(tmp[1][index] || 'null')
      })
      return json
    } else {
      console.log(tmp[2] || null);
      let array = []
      for (let i = 1; i < tmp.length; i++) {
        array.push({})
      }
      let length = (tmp[tmp.length - 1][0] === '') ? tmp.length - 1 : tmp.length
      tmp[0].map((key, index) => {
        for (let i = 1; i < length; i++) {
          array[i - 1][key] = this.parseValue(tmp[i][index] || 'null')
        }
      })
      return array
    }
  }
}
