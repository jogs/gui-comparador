import {basic, nested, deep, full} from 'json-file-validator'

const json = (type) => {
  switch (type) {
    case 'nested':
      return new RegExp(`${/^(?:[\s\n]*)/.source}(${nested.source})`)
    case 'deep':
      return new RegExp(`${/^(?:[\s\n]*)/.source}(${deep.source})`)
    case 'full':
      return new RegExp(`${/^(?:[\s\n]*)/.source}(${full.source})`)
    case 'basic':
    default:
      return new RegExp(`${/^(?:[\s\n]*)/.source}(${basic.source})`)
  }
}

export default json
