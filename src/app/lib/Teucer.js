export default class Teucer {
  constructor (url) {
    this.url = url
    return this
  }
  get (payload = {}) {
    return this.method('GET', this.url, payload)
  }
  post (payload = {}) {
    return this.method('POST', this.url, payload)
  }
  put (payload = {}) {
    return this.method('PUT', this.url, payload)
  }
  delete (payload = {}) {
    return this.method('DELETE', this.url, payload)
  }
  request () {
    let returnEvent = (evt) => evt
    let progressCalc = (evt) => {
      (evt.lengthComputable) ? evt.loaded / evt.total : 0
    }
    let ajax = new XMLHttpRequest()
    ajax.onprogress = progressCalc
    ajax.onload = returnEvent
    ajax.onerror = returnEvent
    ajax.onabort = returnEvent
    ajax.onloadend = returnEvent
    ajax.onloadstart = returnEvent
    ajax.upload.onprogress = progressCalc
    ajax.upload.onload = returnEvent
    ajax.upload.onerror = returnEvent
    ajax.upload.onabort = returnEvent
    ajax.upload.onloadend = returnEvent
    ajax.upload.onloadstart = returnEvent
    ajax.onreadystatechange = () => {
      if (ajax.readyState === 4) return ajax.responseText
    }
    return ajax
  }
  method (method, url, payload) {
    let ajax = this.request()
    return new Promise((resolve, reject) => {
      // console.log(ajax)
      let uri = url
      if (method === 'GET' || method === 'DELETE') {
        uri += '?'
        let argcount = 0
        for (let key in payload) {
          if (payload.hasOwnProperty(key)) {
            if (argcount++) {
              uri += '&'
            }
            uri += `${encodeURIComponent(key)}=${encodeURIComponent(payload[key])}`
          }
        }
        ajax.open(method, uri, true)
        ajax.onreadystatechange = () => {
          if (ajax.readyState === 4) {
            console.log(ajax.status)
          }
        }
        ajax.send()
      } else {
        ajax.open(method, uri, true)
        ajax.setRequestHeader('Content-Type', 'application/json')
        ajax.onreadystatechange = () => {
          if (ajax.readyState === 4) {
            console.log(ajax)
          }
        }
        ajax.send(JSON.stringify(payload))
      }
      ajax.onload = function () {
        if (this.status >= 200 && this.status < 300) {
          resolve(JSON.parse(this.response))
        } else {
          reject(JSON.parse(this.response))
        }
      }
      ajax.onerror = function () {
        reject(this.statusText)
      }
    })
  }
}
