import React, {Component} from 'react'
import Converter from '../../lib/XSVtoJSON'
const FileReader = window.FileReader

export default class OpenFile extends Component {
  constructor (props) {
    super(props)
    this.state = {
      valid: false,
      file: null
    }
    this.handleSelectFile = this.handleSelectFile.bind(this)
    this.handleChangeFile = this.handleChangeFile.bind(this)
  }
  handleSelectFile (event) {
    let self = this
    let file = event.target.files[0]
    let reader = new FileReader()
    reader.onload = function () {
      if (file.type === 'text/csv') {
        let csv = new Converter()
        let result = JSON.stringify(csv.parse(this.result), null, 4)
        if (result[0] === '{') result = `[\n${result}\n]`
        self.setState({file: `{\n"${self.props.name}":\n\t${result}\n}`}, function () {
          self.handleChangeFile(null, null, self.state.file)
        })
      } else {
        self.setState({file: this.result}, function () {
          self.handleChangeFile(null, null, self.state.file)
        })
      }
    }
    reader.readAsText(file)
  }
  handleChangeFile (event, i, v) {
    let value = v || event.target.value || i
    this.setState({
      file: value,
      valid: this.props.validator.test(value)
    }, function () {
      this.props.dataBinding(null, null, this.state.file)
    })
  }
  render () {
    return (
      <input
        type='file'
        multiple={false}
        onChange={this.handleSelectFile}
      />
    )
  }
}

OpenFile.defaultProps = {
  name: 'file'
}
