import React, {Component} from 'react'

export default class ObjectTable extends Component {
  constructor (props) {
    super(props)
    this.state = {
      headers: [],
      data: [],
      selected: false,
      toCompare: []
    }
  }
  render () {
    return (
      <table>
        <thead>
          <tr>
            {
              this.state.headers.map((header) => {
                return <th>{header.toUpperCase()}</th>
              })
            }
          </tr>
        </thead>
        <tbody>
          {
            this.state.data.map((obj) => {
              <tr>
                {
                  this.state.header.map((header) => {
                    if (obj.hasOwnProperty(header)) {
                      return <td>{(typeof obj[header] === 'boolean') ? ((obj[header]) ? 'true' : 'false') : obj[header]}</td>
                    }
                  })
                }
              </tr>
            })
          }
        </tbody>
      </table>
    )
  }
}
