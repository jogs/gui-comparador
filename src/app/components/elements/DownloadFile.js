import React, {Component} from 'react'
const Blob = window.Blob

export default class DownloadFile extends Component {
  constructor (props) {
    super(props)
    this.generate = this.generate.bind(this)
  }
  generate () {
    let file = new Blob([this.props.file], { type: this.props.mime })
    return window.URL.createObjectURL(file)
  }
  render () {
    return (
      <a
        href={this.generate()}
        download={`${this.props.name}_${Date.now()}.${this.props.mime.split('/')[1]}`}
      >
        {`${this.props.prefix} ${this.props.name}`}
      </a>
    )
  }
}

DownloadFile.defaultProps = {
  prefix: 'Download',
  name: 'myFile',
  file: '// empty',
  mime: 'text/txt'
}
