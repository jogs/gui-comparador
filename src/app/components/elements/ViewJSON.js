import React, {Component} from 'react'
import OpenFile from './OpenFile'
import DownloadFile from './DownloadFile'
import getValidator from '../../lib/JSON'
const full = {
  width: '97%'
}

export default class ViewJSON extends Component {
  constructor (props) {
    super(props)
    this.state = {
      valid: true,
      json: {},
      text: '{}'
    }
    this.validator = getValidator(this.props.validator)
    this.handleChange = this.handleChange.bind(this)
  }
  handleChange (event, index, value) {
    let text = value || event.target.value || index
    let valid = this.validator.test(text)
    if (valid) {
      let json = JSON.parse(text.match(this.validator)[0])
      let deepValid = true
      this.props.expected.map((expect) => {
        deepValid = deepValid && json.hasOwnProperty(expect.toLowerCase())
      })
      valid = valid && deepValid
      this.setState({text, valid, json})
    } else {
      this.setState({text, valid, json: {}})
    }
  }
  render () {
    return (
      <div className='row around-xs' style={
        {
          width: '98%',
          position: 'relative'
        }
      }>
        <h3
          className='col-xs-12'
        >{this.props.name}</h3>
        <OpenFile
          className='col-xs-6'
          // width={full}
          validator={this.validator}
          ref='open'
          dataBinding={this.handleChange}
          name={this.props.name}
        />
        {
          (() => {
            if (this.state.valid) {
              return <DownloadFile
                className='col-xs-6 download-btn'
                // width={full}
                ref='download'
                name={this.props.name}
                file={this.state.text}
                mime='application/json'
                prefix='Descarga'
              />
            }
          })()
        }
        <textarea
          width={full}
          className={(this.state.valid) ? 'valid-text col-xs-12' : 'wrong-text col-xs-12'}
          value={this.state.text}
          onChange={this.handleChange}
          rows={this.props.rows}
        ></textarea>
        <button
          className='col-xs-10'
          onClick={() => this.handleChange(null, null, JSON.stringify(this.props.template, null, 4))}
        >
          Usar plantilla
        </button>
      </div>
    )
  }
}

ViewJSON.defaultProps = {
  rows: 25,
  validator: 'nested',
  expected: [],
  name: 'example',
  template: {}
}
