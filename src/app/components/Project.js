import React, {Component} from 'react'
import ViewJSON from './elements/ViewJSON'
import Teucer from '../lib/Teucer'

export default class Project extends Component {
  constructor (props) {
    super(props)
    this.state = {
      basics: false,
      profile: {
        esquema: {},
        comparacion: {},
        semejanza: {}
      },
      comparation: false,
      headers: [],
      objects: [],
      o1: 0,
      o2: 0,
      selected: false,
      result: {}
    }
    this.api = `http://${window.location.hostname}:19170`
    this.comparer = new Teucer(`${this.api}/compare`)
    this.helper = new Teucer(`${this.api}/functions`)
    this.handleObjectSelect = this.handleObjectSelect.bind(this)
  }
  handleObjectSelect (o, index) {
    let tmp = {result: {}}
    tmp[o] = index
    // console.log(tmp)
    this.setState(tmp)
  }
  render () {
    return (
      <section id='Project-Section' className='row center-xs'>
        <h2 className='col-xs-11'>Projecto</h2>
        {
          (() => {
            if (this.state.comparation) {
              return (
                <div>
                  <div className='col-xs-12'>
                    <input
                      type='number'
                      value={this.state.o1}
                      min={0}
                      max={this.state.headers.length - 1}
                      className='o1'
                      onChange={(e, i, value) => this.handleObjectSelect('o1', value || e.target.value || i)}
                      style={{width: '49%', display: 'inline'}}
                    />
                    <input
                      type='number'
                      value={this.state.o2}
                      min={0}
                      max={this.state.headers.length - 1}
                      className='o2'
                      onChange={(e, i, value) => this.handleObjectSelect('o2', value || e.target.value || i)}
                      style={{width: '49%', display: 'inline'}}
                    />
                    <button
                      onClick={
                        () => {
                          let data = Object.assign({}, this.state.profile)
                          data.objetos = [
                            this.state.objects[this.state.o1], this.state.objects[this.state.o2]
                          ]
                          this.comparer.post(data).then((result) => {
                            this.setState({result: result.body})
                          }).catch((err) => {
                            console.error(err)
                            window.alert(JSON.stringify(err))
                          })
                        }
                      }
                    >
                      Comparar
                    </button>
                  </div>
                  <div className='col-xs-12 col-md-6'>
                    {
                      (() => {
                        if (this.state.result.hasOwnProperty('semejanza')) {
                          return <h3>Semejanza {this.state.result.semejanza}</h3>
                        }
                      })()
                    }
                    <table>
                      <caption>Objetos a comparar</caption>
                      <thead>
                        <tr>
                          {
                            this.state.headers.map((header) => {
                              return <th key={`${header}-comp`}>{header.toUpperCase()}</th>
                            })
                          }
                        </tr>
                      </thead>
                      <tbody>
                        <tr className='o1'>
                          {
                            this.state.headers.map((header) => {
                              return (
                                <td key={`${header}-comp-o1`}>{(typeof this.state.objects[this.state.o1][header] === 'boolean') ? ((this.state.objects[this.state.o1][header]) ? 'true' : 'false') : this.state.objects[this.state.o1][header]}</td>
                              )
                            })
                          }
                        </tr>
                        <tr className='o2'>
                          {
                            this.state.headers.map((header) => {
                              return (
                                <td key={`${header}-comp-o2`}>{(typeof this.state.objects[this.state.o2][header] === 'boolean') ? ((this.state.objects[this.state.o2][header]) ? 'true' : 'false') : this.state.objects[this.state.o2][header]}</td>
                              )
                            })
                          }
                        </tr>
                      </tbody>
                      {
                        (() => {
                          if (this.state.result.hasOwnProperty('resultado')) {
                            return (<tfoot>
                              <tr>
                                {
                                  this.state.headers.map((header) => {
                                    return <td key={`${header}-result`}>{this.state.result.resultado[header.toLowerCase()]}</td>
                                  })
                                }
                              </tr>
                            </tfoot>)
                          }
                        })()
                      }
                    </table>
                  </div>
                  <div className='col-xs-12 col-md-6'>
                    <table>
                      <caption>Lista de Objetos</caption>
                      <thead>
                        <tr>
                          <th>#</th>
                          {
                            this.state.headers.map((header) => {
                              return <th key={header}>{header.toUpperCase()}</th>
                            })
                          }
                        </tr>
                      </thead>
                      <tbody>
                        {
                          this.state.objects.map((obj, index) => {
                            return (<tr key={index}>
                              <td>{index}</td>
                              {
                                this.state.headers.map((header) => {
                                  if (obj.hasOwnProperty(header)) {
                                    // console.log(obj[header])
                                    return (<td key={`${header}-${index}`}>{(typeof obj[header] === 'boolean') ? ((obj[header]) ? 'true' : 'false') : obj[header]}</td>)
                                  }
                                })
                              }
                            </tr>)
                          })
                        }
                      </tbody>
                    </table>
                  </div>
                </div>
              )
            } else {
              return (
                <div style={{width: '100%', position: 'relative'}}>
                  <div className='box col-xs-12'>
                    <button
                      className='col-xs-11'
                      onClick={
                        () => this.setState({basics: !this.state.basics})
                      }
                    >Basicos</button>
                    <button
                      className='col-xs-11'
                      onClick={
                        () => this.helper.get().then((funcs) => {
                          window.confirm(`CC: ${funcs.body.cc}`)
                          window.confirm(`SF: ${funcs.body.sf}`)
                        }).catch((err) => window.alert(JSON.stringify(err)))
                      }
                    >Funciones disponibles</button>
                    {(() => {
                      if (this.state.basics) {
                        return (
                          <div className='row around-xs' style={{width: '100%'}}>
                            <div className='col-xs-12 col-md-4'>
                              <ViewJSON
                                name='esquema'
                                ref='esquema'
                                expected={['esquema']}
                                template={{
                                  esquema: [
                                    {
                                      nombre: 'activo'
                                    },
                                    {
                                      nombre: 'Sexo',
                                      variable: 'k-valente',
                                      valores: ['Masculino', 'Femenino']
                                    },
                                    {
                                      nombre: 'Edad',
                                      variable: 'real',
                                      valores: '[0, 120]'
                                    }
                                  ]
                                }}
                              />
                            </div>
                            <div className='col-xs-12 col-md-4'>
                              <ViewJSON
                                name='comparacion'
                                ref='comparacion'
                                expected={['comparacion']}
                                template={{
                                  comparacion: {
                                    'sexo': {
                                      criterio: 'igualdad'
                                    },
                                    'edad': {
                                      criterio: 'umbral',
                                      diferencia: true,
                                      datos: 5
                                    }
                                  }
                                }}
                              />
                            </div>
                            <div className='col-xs-12 col-md-4'>
                              <ViewJSON
                                name='semejanza'
                                ref='semejanza'
                                expected={['semejanza']}
                                template={{
                                  semejanza: {
                                    funcion: 'porcentaje',
                                    diferencia: false,
                                    datos: 66.7
                                  }
                                }}
                              />
                            </div>
                            <br/>
                            <button
                              style={
                                {
                                  margin: '10px 0px'
                                }
                              }
                              className='col-xs-11'
                              onClick={
                                () => {
                                  let obj = Object.assign({}, this.refs.Esquema.state.json, this.refs.Comparacion.state.json, this.refs.Semejanza.state.json)
                                  this.refs.Perfil.handleChange(null, null, JSON.stringify(obj, null, 4))
                                }
                              }
                            >Compilar</button>
                            <br/>
                          </div>
                        )
                      }
                    })()}
                  </div>
                  <div className='col-xs-11 col-md-5' style={{display: 'inline-flex'}}>
                    <ViewJSON
                      name='perfil'
                      ref='perfil'
                      expected={['esquema', 'semejanza', 'comparacion']}
                      template={{
                        esquema: [
                          {
                            nombre: 'activo'
                          },
                          {
                            nombre: 'Sexo',
                            variable: 'k-valente',
                            valores: ['Masculino', 'Femenino']
                          },
                          {
                            nombre: 'Edad',
                            variable: 'real',
                            valores: '[0, 120]'
                          }
                        ],
                        comparacion: {
                          'sexo': {
                            criterio: 'igualdad'
                          },
                          'edad': {
                            criterio: 'umbral',
                            diferencia: true,
                            datos: 5
                          }
                        },
                        semejanza: {
                          funcion: 'porcentaje',
                          diferencia: false,
                          datos: 66.7
                        }
                      }}
                    />
                  </div>
                  <div className='col-xs-11 col-md-5' style={{display: 'inline-flex'}}>
                    <ViewJSON
                      name='objetos'
                      ref='objetos'
                      expected={['objetos']}
                      template={{
                        objetos: [
                          {
                            sexo: 'Masculino',
                            edad: 46
                          },
                          {
                            sexo: 'masculino',
                            edad: 18
                          }
                        ]
                      }}
                    />
                  </div>
                  <br/>
                  <button
                    style={{display: 'inline-flex', width: '80%', margin: '10px 0px'}}
                    className='col-xs-11'
                    onClick={
                      () => {
                        if (this.refs.hasOwnProperty('perfil') && this.refs.hasOwnProperty('objetos')) {
                          let perfil = this.refs.perfil.state.json
                          let objetos = this.refs.objetos.state.json
                          if (perfil.hasOwnProperty('esquema') && perfil.hasOwnProperty('semejanza') && perfil.hasOwnProperty('comparacion') && objetos.hasOwnProperty('objetos')) {
                            let keys = Object.keys(perfil.comparacion)

                            this.setState({
                              profile: {
                                esquema: perfil.esquema,
                                comparacion: perfil.comparacion,
                                semejanza: perfil.semejanza
                              },
                              objects: objetos.objetos,
                              headers: keys
                            })
                            this.setState({comparation: true})
                          } else {
                            window.alert('Algun Elemento no cuenta con la informacion necesaria, por favor verifique la documentación')
                          }
                        } else {
                          console.log(':(')
                        }
                      }
                    }
                  >Confirmar</button>
                </div>
              )
            }
          })()
        }
      </section>
    )
  }
}
