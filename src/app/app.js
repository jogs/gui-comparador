import React, {Component} from 'react'
import {render} from 'react-dom'
import Project from './components/Project'

class App extends Component {
  constructor (props) {
    super(props)
  }
  render () {
    return (
      <div>
        <h1>Comparador</h1>
        <Project />
      </div>
    )
  }
}

if (window.sessionStorage && window.File && window.FileReader && window.FileList) {
  window.sessionStorage.setItem('apiUrl', `http://${window.location.hostname}:19170`)
  render((
    <App />
  ), document.getElementById('app'))
} else {
  window.alert('Su navegador no soporta HTML5, se sugiere "Google Chrome" o "Mozilla Firefox"')
}
